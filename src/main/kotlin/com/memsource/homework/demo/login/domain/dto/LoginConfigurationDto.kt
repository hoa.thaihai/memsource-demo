package com.memsource.homework.demo.login.domain.dto

data class LoginConfigurationDto(
        val username: String,
        val password: String
)