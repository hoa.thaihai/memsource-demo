package com.memsource.homework.demo.login.repository

import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import org.springframework.data.jpa.repository.JpaRepository

interface LoginConfigurationRepository : JpaRepository<LoginConfiguration, Long> {

    fun findFirstByOrderById(): LoginConfiguration?
}