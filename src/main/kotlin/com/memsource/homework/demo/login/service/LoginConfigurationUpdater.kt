package com.memsource.homework.demo.login.service

import com.memsource.homework.demo.login.domain.dto.LoginConfigurationDto
import com.memsource.homework.demo.login.domain.dto.TokenDto
import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import com.memsource.homework.demo.login.repository.LoginConfigurationRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class LoginConfigurationUpdater(
        private val loginConfigurationRepository: LoginConfigurationRepository,
        private val loginService: LoginService
) {

    @Transactional
    fun updateOrCreate(update: LoginConfigurationDto): LoginConfiguration {
        val newToken = loginService.login(update.username, update.password)
        return loginConfigurationRepository.findFirstByOrderById()?.apply {
            username = update.username
            password = update.password
            token = newToken.token
            tokenExpires = newToken.expires
        } ?: update.saveNew(newToken)
    }

    private fun LoginConfigurationDto.saveNew(newToken: TokenDto) = loginConfigurationRepository.save(
            LoginConfiguration(
                    username = username,
                    password = password,
                    token = newToken.token,
                    tokenExpires = newToken.expires
            )
    )
}
