package com.memsource.homework.demo.login.service

import com.memsource.homework.demo.login.domain.dto.CredentialsDto
import com.memsource.homework.demo.login.domain.dto.TokenDto
import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import com.memsource.homework.demo.login.repository.LoginConfigurationRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForObject
import java.time.ZonedDateTime

@Service
class LoginService(
        private val memsourceRestTemplate: RestTemplate,
        private val loginConfigurationRepository: LoginConfigurationRepository
) {

    fun login(username: String, password: String): TokenDto =
            memsourceRestTemplate.postForObject("/auth/login", CredentialsDto(username, password))

    @Transactional
    fun getToken(): TokenDto {
        val configuration = checkNotNull(loginConfigurationRepository.findFirstByOrderById()) {
            "Credentials not configured"
        }
        return if (configuration.hasExpiredToken()) {
            val newToken = login(configuration.username, configuration.password)
            configuration.token = newToken.token
            configuration.tokenExpires = newToken.expires
            newToken
        } else {
            TokenDto(configuration.token, configuration.tokenExpires)
        }
    }

    private fun LoginConfiguration.hasExpiredToken() = tokenExpires.isBefore(ZonedDateTime.now())
}