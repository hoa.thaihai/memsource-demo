package com.memsource.homework.demo.login.controller

import com.memsource.homework.demo.login.domain.dto.LoginConfigurationDto
import com.memsource.homework.demo.login.service.LoginConfigurationUpdater
import org.springframework.http.HttpStatus.UNAUTHORIZED
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/configuration")
class LoginConfigurationController(private val loginConfigurationUpdater: LoginConfigurationUpdater) {

    @PutMapping
    fun updateLoginConfiguration(@RequestBody loginConfiguration: LoginConfigurationDto) =
            try {
                loginConfigurationUpdater.updateOrCreate(loginConfiguration)
            } catch (e: HttpClientErrorException) {
                throw ResponseStatusException(UNAUTHORIZED, e.message)
            }
}