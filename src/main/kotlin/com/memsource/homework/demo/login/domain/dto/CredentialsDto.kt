package com.memsource.homework.demo.login.domain.dto

data class CredentialsDto(
        val userName: String,
        val password: String,
        val code: String? = null
)