package com.memsource.homework.demo.login.domain.entity

import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "login_configuration")
data class LoginConfiguration(
        @Id
        @GeneratedValue
        var id: Long? = null,

        @Column(nullable = false)
        var username: String,

        @Column(nullable = false)
        var password: String,

        @Column(nullable = false)
        var token: String,

        @Column(nullable = false)
        var tokenExpires: ZonedDateTime
)