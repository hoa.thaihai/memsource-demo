package com.memsource.homework.demo.project.service

import com.memsource.homework.demo.login.service.LoginService
import com.memsource.homework.demo.project.domain.dto.ProjectPageDto
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject

@Service
class ProjectService(
        private val memsourceRestTemplate: RestTemplate,
        private val loginService: LoginService
) {

    fun fetchProjects(pageSize: Int, pageNumber: Int): ProjectPageDto? {
        val token = loginService.getToken().token
        return memsourceRestTemplate
                .getForObject<ProjectPageDto>("/projects?token=$token&pageSize=$pageSize&pageNumber=$pageNumber")
    }
}