package com.memsource.homework.demo.project.domain.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProjectDto(
        val name: String,
        val status: String,
        val sourceLang: String,
        val targetLangs: List<String>
)