package com.memsource.homework.demo.project.controller

import com.memsource.homework.demo.project.domain.dto.ProjectPageDto
import com.memsource.homework.demo.project.service.ProjectService
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/projects")
class ProjectController(private val projectService: ProjectService) {

    @GetMapping
    fun getProjects(
            @RequestParam(required = false, defaultValue = "20") pageSize: Int = 20,
            @RequestParam(required = false, defaultValue = "0") pageNumber: Int = 0
    ): ProjectPageDto? = try {
        projectService.fetchProjects(pageSize, pageNumber)
    } catch (e: IllegalStateException) {
        throw ResponseStatusException(BAD_REQUEST, e.message)
    }
}