package com.memsource.homework.demo.project.domain.dto

data class ProjectPageDto(
        val totalElements: Int,
        val totalPages: Int,
        val pageSize: Int,
        val pageNumber: Int,
        val numberOfElements: Int,
        val content: List<ProjectDto>
)