package com.memsource.homework.demo.login.service

import com.memsource.homework.demo.login.domain.dto.TokenDto
import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import com.memsource.homework.demo.login.repository.LoginConfigurationRepository
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.postForObject
import java.time.ZonedDateTime

@DataJpaTest
@Import(LoginService::class)
internal class LoginServiceTest @Autowired constructor(
        private val loginService: LoginService,
        private val loginConfigurationRepository: LoginConfigurationRepository
) {

    @MockkBean
    private lateinit var restTemplate: RestTemplate
    private val newToken = TokenDto(token = "new_token", expires = ZonedDateTime.now().plusDays(1))


    @BeforeEach
    fun mockRestService() {
        every { restTemplate.postForObject<TokenDto>(any<String>(), any()) } returns newToken
    }

    @Test
    fun `Should be able to call login`() {
        assertThat(loginService.login("name", "pass")).isEqualTo(newToken)
    }

    @Test
    fun `Should return saved token`() {
        val existing = LoginConfiguration(
                username = "name",
                password = "pass",
                token = "old_token",
                tokenExpires = ZonedDateTime.now().plusHours(1)
        )
        loginConfigurationRepository.save(existing)

        val result = loginService.getToken()

        assertThat(result.token).isEqualTo(existing.token)
        assertThat(result.expires).isEqualTo(existing.tokenExpires)
    }

    @Test
    fun `Should return new token`() {
        val existing = LoginConfiguration(
                username = "name",
                password = "pass",
                token = "old_token",
                tokenExpires = ZonedDateTime.now().minusSeconds(1)
        )
        loginConfigurationRepository.save(existing)

        assertThat(loginService.getToken()).isEqualTo(newToken)
    }
}