package com.memsource.homework.demo.login.service

import com.memsource.homework.demo.login.domain.dto.LoginConfigurationDto
import com.memsource.homework.demo.login.domain.dto.TokenDto
import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import com.memsource.homework.demo.login.repository.LoginConfigurationRepository
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import java.time.ZonedDateTime

@DataJpaTest
@Import(LoginConfigurationUpdater::class)
internal class LoginConfigurationUpdaterTest @Autowired constructor(
        private val loginConfigurationUpdater: LoginConfigurationUpdater,
        private val loginConfigurationRepository: LoginConfigurationRepository
) {

    @MockkBean
    private lateinit var loginService: LoginService
    private val validToken = TokenDto(token = "valid_token", expires = ZonedDateTime.now().plusDays(1))

    @BeforeEach
    fun mockLoginService() {
        every { loginService.login(any(), any()) } returns validToken
    }

    @Test
    fun `Should save new configuration`() {
        val input = LoginConfigurationDto(username = "name", password = "pass")

        loginConfigurationUpdater.updateOrCreate(input)

        val result = loginConfigurationRepository.findFirstByOrderById()
        assertThat(result).isNotNull
        with(result!!) {
            assertThat(password).isEqualTo(input.password)
            assertThat(username).isEqualTo(input.username)
            assertThat(token).isEqualTo(validToken.token)
            assertThat(tokenExpires).isEqualTo(validToken.expires)
        }
    }

    @Test
    fun `Should update existing configuration`() {
        val existing = LoginConfiguration(username = "name", password = "old_pass", token = "token", tokenExpires = ZonedDateTime.now())
        loginConfigurationRepository.save(existing)
        val input = LoginConfigurationDto(username = "name", password = "new_pass")

        loginConfigurationUpdater.updateOrCreate(input)

        val result = loginConfigurationRepository.findFirstByOrderById()
        assertThat(result).isNotNull
        with(result!!) {
            assertThat(password).isEqualTo(input.password)
            assertThat(username).isEqualTo(input.username)
            assertThat(token).isEqualTo(validToken.token)
            assertThat(tokenExpires).isEqualTo(validToken.expires)
        }
    }
}