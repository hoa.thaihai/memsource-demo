package com.memsource.homework.demo.login.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.memsource.homework.demo.login.domain.dto.LoginConfigurationDto
import com.memsource.homework.demo.login.domain.entity.LoginConfiguration
import com.memsource.homework.demo.login.service.LoginConfigurationUpdater
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.put
import java.time.ZonedDateTime

@WebMvcTest
internal class LoginConfigurationControllerTest(@Autowired private val mockMvc: MockMvc) {

    @MockkBean
    private lateinit var loginConfigurationUpdater: LoginConfigurationUpdater

    @Test
    fun `Update login configuration`() {
        every { loginConfigurationUpdater.updateOrCreate(any()) } returns
                LoginConfiguration(username = "name", password = "pass", token = "token", tokenExpires = ZonedDateTime.now())
        val payload = jacksonObjectMapper().writeValueAsString(LoginConfigurationDto(username = "name", password = "pass"))

        mockMvc.put("/api/configuration") {
            content = payload
            contentType = APPLICATION_JSON
        }.andExpect {
            status { is2xxSuccessful }
            content { json(payload) }
        }
    }
}