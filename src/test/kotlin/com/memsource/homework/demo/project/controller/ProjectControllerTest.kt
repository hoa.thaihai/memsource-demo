package com.memsource.homework.demo.project.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.memsource.homework.demo.project.domain.dto.ProjectDto
import com.memsource.homework.demo.project.domain.dto.ProjectPageDto
import com.memsource.homework.demo.project.service.ProjectService
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest(ProjectController::class)
internal class ProjectControllerTest(@Autowired private val mockMvc: MockMvc) {

    @MockkBean
    private lateinit var projectService: ProjectService

    @Test
    fun `Update login configuration`() {
        val projectPage = ProjectPageDto(totalElements = 1, totalPages = 1, pageNumber = 0, pageSize = 10, numberOfElements = 1, content = listOf(
                ProjectDto(name = "project", status = "NEW", sourceLang = "cs", targetLangs = listOf("en"))
        ))
        every { projectService.fetchProjects(any(), any()) } returns
                projectPage
        val payload = jacksonObjectMapper().writeValueAsString(projectPage)

        mockMvc.get("/api/projects")
                .andExpect {
                    status { is2xxSuccessful }
                    content { json(payload) }
                }
    }
}