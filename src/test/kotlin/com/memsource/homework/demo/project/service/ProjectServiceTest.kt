package com.memsource.homework.demo.project.service

import com.memsource.homework.demo.login.domain.dto.TokenDto
import com.memsource.homework.demo.login.service.LoginService
import com.memsource.homework.demo.project.domain.dto.ProjectDto
import com.memsource.homework.demo.project.domain.dto.ProjectPageDto
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.web.client.RestTemplate
import java.time.ZonedDateTime

@DataJpaTest
@Import(ProjectService::class)
internal class ProjectServiceTest(@Autowired private val projectService: ProjectService) {

    @MockkBean
    private lateinit var restTemplate: RestTemplate

    @MockkBean
    private lateinit var loginService: LoginService
    private val projectPage = ProjectPageDto(totalElements = 1, totalPages = 1, pageNumber = 0, pageSize = 10, numberOfElements = 1, content = listOf(
            ProjectDto(name = "project", status = "NEW", sourceLang = "cs", targetLangs = listOf("en"))
    ))

    @BeforeEach
    fun mockRestService() {
        every { restTemplate.getForObject<ProjectPageDto>(any<String>(), any()) } returns projectPage
        every { loginService.getToken() } returns TokenDto(token = "token", expires = ZonedDateTime.now())
    }

    @Test
    fun `Should fetch project page`() {
        assertThat(projectService.fetchProjects(0, 10)).isEqualTo(projectPage)
    }
}