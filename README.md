# Memsource Demo

Simple Spring Boot app with VueJs.

---

### Requirements
* JRE 

### How to run
1. ```./mvnw spring-boot:run```
2. <http://localhost:8080/>